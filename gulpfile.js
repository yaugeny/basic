var gulp       = require('gulp'),
    sass       = require('gulp-sass'),
    minifyCSS  = require('gulp-minify-css'),
    rename     = require('gulp-rename'),
    uglify     = require('gulp-uglify'),
    concat     = require('gulp-concat'),
    connect    = require('gulp-connect'),
    sourcemaps = require('gulp-sourcemaps'),
    iconv      = require('gulp-iconv'),
    clean      = require('gulp-clean'),
    replace    = require('gulp-replace'),
    zip        = require('gulp-zip'),
    check      = require('gulp-if'),
    filter     = require('gulp-filter'),
    jade       = require('gulp-jade'),
    args       = require('yargs').argv,
    jpegoptim  = require('imagemin-jpegoptim'),
    pngquant   = require('imagemin-pngquant'),
    optipng    = require('imagemin-optipng'),
    svgo       = require('imagemin-svgo');

var paths = {

    src: {
        sass: [
            './src/sass/app.scss',
        ],
        js: [
            './bower_components/nstSlider/dist/jquery.nstSlider.js',
            './bower_components/jQuery Nice Select/js/jquery.nice-select.js',
            './src/js/**/*.js',
        ],
        fonts: [
            './src/fonts/*',
            './bower_components/fontawesome/fonts/*',
            './bower_components/bootstrap-sass/assets/fonts/bootstrap/*',
        ],
        images: [
            './src/images/**/*.{png,jpg,jpeg,gif,svg}',
        ],
        jade: [
            './src/jade/*.jade',
        ],

    },

    build: {

        all:    './build/**',
        base:   './build',
        css:    './build/css',
        js:     './build/js',
        fonts:  './build/fonts',
        images: './build/images',

    },

    production: {

        clean: [
            './build/maps'
        ]

    }
};

// Files

gulp.task('css', function () {
    return gulp.src(paths.src.sass)
        .pipe(check(!args.production, sourcemaps.init()))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(paths.build.css))
        .pipe(minifyCSS())
        .pipe(rename({suffix: '.min'}))
        .pipe(check(!args.production, sourcemaps.write('../maps')))
        .pipe(gulp.dest(paths.build.css));
});

gulp.task('js', function() {
    return gulp.src(paths.src.js)
        .pipe(check(!args.production, sourcemaps.init()))
        .pipe(concat('app.js'))
        .pipe(gulp.dest(paths.build.js))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(check(!args.production, sourcemaps.write('../maps')))
        .pipe(gulp.dest(paths.build.js));
});

gulp.task('fonts', function() {
    return gulp.src(paths.src.fonts)
        .pipe(gulp.dest(paths.build.fonts));
});

gulp.task('images', function () {
    return gulp.src(paths.src.images)
        .pipe(pngquant({quality: '100', speed: 4})())
        .pipe(optipng({optimizationLevel: 3})())
        .pipe(jpegoptim({max: 100})())
        .pipe(svgo()())
        .pipe(gulp.dest(paths.build.images));
});

gulp.task('jade', function() {
    gulp.src(paths.src.jade)
        .pipe(jade({
            pretty: true
        }))
        .pipe(gulp.dest(paths.build.base))
});

gulp.task('default', ['css', 'js', 'fonts', 'images', 'jade'], function(){
    if (args.production) {
        return gulp.src(paths.production.clean, {read: false})
            .pipe(clean());
    }
});

// Watch

gulp.task('connect', function() {
    connect.server({
        root: './build',
        livereload: true
    });
});

gulp.task('reload', function () {
    return gulp.src('./build/*.html')
        .pipe(connect.reload());
});

gulp.task('watch', function() {
    gulp.watch(['./build/**/*.html', './build/js/**/*.js', './build/css/**/*.css'], ['reload']);
    gulp.watch(['./src/sass/**/*.sass', './src/sass/**/*.scss'], ['css']);
    gulp.watch(['./src/js/**/*.js'], ['js']);
    gulp.watch(['./src/jade/**/*.jade'], ['jade']);
});

gulp.task('run', ['connect', 'watch']);

// cp1251

gulp.task('cp1251', ['default'], function () {

    var replaceFilter = filter('**/*.{html,css}', {restore: true}),
        iconvFilter   = filter('**/*.{html,css,js,txt}', {restore: true});

    var folders     = __dirname.split('/'),
        projectName = folders[folders.length - 1];

    return gulp.src(paths.build.all)
        .pipe(replaceFilter)
        .pipe(replace(/utf-8/i, 'windows-1251'))
        .pipe(replaceFilter.restore)
        .pipe(iconvFilter)
        .pipe(iconv({
            decoding: 'utf-8',
            encoding: 'windows-1251'
        }))
        .pipe(iconvFilter.restore)
        .pipe(gulp.dest('./build_cp1251'))
        .pipe(zip(projectName + '_cp1251.zip'))
        .pipe(gulp.dest('./'));

});